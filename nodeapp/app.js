//declare dependencies

const express = require("express");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer");

//connect to DB
mongoose.connect("mongodb://localhost:27017/mern_tracker2", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
});

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});

//Apply Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Declare Model
const Team = require("./models/teams");
const Member = require("./models/members");
const Task = require("./models/tasks")

//Create Routes/Endpoints

//Trasferred Routes

//Declare the resources
const teamsRoute = require("./routes/teams");
app.use("/teams", teamsRoute);

const tasksRoute = require("./routes/tasks");
app.use("/tasks", tasksRoute);

const membersRoute = require("./routes/members");
app.use("/members", membersRoute);


//CONFIGURE MULTER

const upload = multer({
	dest: "images/members",
	limits: {
		fileSize: 1000000 //max file size in bytes
	},
	fileFilter(req, file, cb) {
		//https://regex101.com
		if(!file.originalname.match(/\.(jpg|jpeg|png|PNG)$/)){ //escape regular expression characters /\.()&/)
			return cb(new Error("Please upload an image only"))
		}
		cb(undefined, true)
	}

});

//SAMPLE: ENDPOINT TO UPLOAD A FILE
app.post("/upload", upload.single("label-upload"), (req, res) =>{
	try{
		res.send({message: "Successfully uploaded image!"})
	}catch(e){
		//BAD REQUEST
		res.status(400).send({error: e.message})
	}
});


//initialize the server

app.listen(5000, () => {
	console.log("Now listening to port 5000");
});